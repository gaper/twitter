
using System;

namespace twiter.DTOs
{
    public class Comentarios

    {
        
        public string Id { get; set; }

        public string idUsuario { get; set; }
        
        public string texto { get; set; }
        
        public int meGusta { get; set; }
        
        public int noMeGusta { get; set; }
    }


}