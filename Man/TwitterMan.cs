using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using twiter.DAL;
using twiter.DTOs;



namespace twiter.Man
{

    public class TwitterMan
    {

        private TwitterDAL tDAL;
        public TwitterMan()
        {
            this.tDAL = new TwitterDAL();
        }

        public async Task<bool> NuevoUsuario(string mail)
        {
            try
            {
                return await this.tDAL.InsertUsuario(mail);
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<string> ExisteUsuario(string mail)
        {

            try
            {
                return await this.tDAL.ExistUsuario(mail);
            }
            catch (Exception e)
            {

                throw e;
            }

        }

        public async Task<bool> AgregarComentario(string idUser, string text)
        {

            try
            {
                Comentario coment = new Comentario
                {
                    idUsuario = idUser,
                    texto = text,
                    meGusta = 0,
                    noMeGusta = 0
                };

                return await this.tDAL.InsertComentario(coment);
            }
            catch (Exception e)
            {

                throw e;

            }

        }


        public async Task<ComentarioRetorno> ListComentariosUsuario(string iduser)
        {

            try
            {
                ComentarioRetorno comentRet = new ComentarioRetorno();
                
                comentRet.IdUsuario = iduser;
                comentRet.Comentarios = new List<ComentarioForRet>();
               


                List<Comentarios> coments = await this.tDAL.ObtenerComentariosUsuario(iduser);

                if (coments != null)
                {
                    foreach (var item in coments)
                    {
                        ComentarioForRet ComForRet = new ComentarioForRet
                        {
                            Id = item.Id,    
                            texto = item.texto,
                            meGusta = item.meGusta,
                            noMeGusta = item.noMeGusta
                        };

                        comentRet.Comentarios.Add(ComForRet);
                    }

                        return comentRet;
                }
                else
                {

                    return null;
                }
            }
            catch (Exception e)
            {
                
                throw e;
            }



        }
        public async Task<bool> AddEmocionComentario(string IdComentario, bool emocion)
        {
            try
            {
                return await this.tDAL.AddEmocionComentario(IdComentario, emocion);
            }
            catch (Exception e)
            {

                throw e;
            }


        }

        public async Task<Comentarios> ReadComentario(string IdComentario)
        {
            try
            {
                return await this.tDAL.SlectComentario(IdComentario);
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}
