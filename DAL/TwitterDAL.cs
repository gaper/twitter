using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using twiter.DTOs;
using Raven.Client.Documents;



namespace twiter.DAL
{
    public class TwitterDAL
    {
        private DocumentStore store;

        public TwitterDAL()
        {

            store = new DocumentStore
            {
                Urls = new[] { "http://localhost:8081" },
                Database = "TwitterDB"
            };

            store.Initialize();
        }

        public async Task<bool> InsertUsuario(string pmail)
        {

            using (var session = store.OpenSession())
            {
                Usuario user = new Usuario
                {
                    mail = pmail
                };

                session.Store(user);
                session.SaveChanges();
            }

            return true;
        }

        public async Task<string> ExistUsuario(string mail)
        {
            
            string idUser = "";

            using (var session = store.OpenSession())
            {
                var Users =
                from p in session.Query<Usuarios>()
                where p.mail == mail
                select p;
            
            if (Users != null)
            {

                foreach (var s in Users)
                {

                    idUser = s.Id;
                }
            }
            
            }
           
            return idUser;

        }

        public async Task<bool> InsertComentario(Comentario coment)
        {
            using (var session = store.OpenSession())
            {
                session.Store(coment);
                session.SaveChanges();

            }

            return true;
        }

        public async Task<List<Comentarios>> ObtenerComentariosUsuario(string idUser)
        {

            List<Comentarios> Coments;
            

            using (var session = store.OpenSession())
            {
                var com  =
                from c in session.Query<Comentarios>()
                where c.idUsuario == idUser
                select c;

                Coments = com.ToList();
            
            }
            
            return Coments;
        }

        public async Task<bool> AddEmocionComentario(string idComentario, bool emo)
        {

            Comentario coment = null;

            using (var session = store.OpenSession())
            {
                coment = session.Load<Comentario>(idComentario);


                if (emo)
                {

                    coment.meGusta = coment.meGusta + 1;

                }
                else
                {

                    coment.noMeGusta = coment.noMeGusta + 1;
                }

                session.SaveChanges();
            }

            return true;
        }

        public async Task<Comentarios> SlectComentario(string idComentario)
        {

            Comentarios coment = null;

            using (var session = store.OpenSession())
            {
                coment = session.Load<Comentarios>(idComentario);

            }

            return coment;

        }
    }
}
