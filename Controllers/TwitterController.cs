using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using twiter.Models;
using twiter.Man;
using twiter.DTOs;

namespace twiter.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    
    public class TwitterController : Controller
    {
        private TwitterMan tMan;
        private string idUser;

        public TwitterController()
        {

            this.tMan = new TwitterMan();
            this.idUser = "";
        }

        [HttpPost("crearusuario")]
        public async Task<IActionResult> CrearUsuario(Usuario user)
        {
            idUser = await this.tMan.ExisteUsuario(user.mail);

            if (string.IsNullOrEmpty(idUser))
            {
                bool cUser = false;

                cUser = await this.tMan.NuevoUsuario(user.mail);

                if(cUser){
                    return Json("Usuario agregado correctamente!");
                }else{

                    return Json("Ha ocurrido un error vuelva a intentarlo!");
                }

            }

            return Json("El usuario ya existe en el sistema!");

        }
        [HttpPost("crearcomentario")]
        public async Task<IActionResult> CrearComentario(ComentarioIn coment)
        {
             idUser = await this.tMan.ExisteUsuario(coment.mailUsuario);

            if (!string.IsNullOrEmpty(idUser)){

               if(coment.texto.Length < 256){     
                   if(await this.tMan.AgregarComentario(idUser, coment.texto)){
                       return Json("Comentario agregado correctamente!");
                   }else{
                       return Json("Ha ocurrido un error vuelva a intentarlo!");
                   }
                    
               }else{
                   return Json("El comentario supera los 256 caracteres");
               }
            
            }else{
                return Json("El usuario no existe!");
            } 

        }

        [HttpGet("listarcomentarios")]
        public async Task<IActionResult> ListarComentariosUsuario(Usuario user)
        {
            idUser = await this.tMan.ExisteUsuario(user.mail);

            if (!string.IsNullOrEmpty(idUser)){
 
                return Json(await this.tMan.ListComentariosUsuario(idUser));
            
            }else{
                return Json("El usuario no existe!");
            } 


        }

        [HttpPut("addemocion")]
        public async Task<IActionResult> AgregarEmocionComentario(EmocionIn emo)
        {
               idUser = await this.tMan.ExisteUsuario(emo.mailUsuario);

            if (!string.IsNullOrEmpty(idUser)){

                return Json(await this.tMan.AddEmocionComentario(emo.IdComentario,emo.emocion));
            
            }else{
                return Json("El usuario no existe!");
            } 
        }

        [HttpGet("leercomentario")]
        public async Task<IActionResult> LeerComentario(ObtenerComentIn coment)
        {
                return Json(await this.tMan.ReadComentario(coment.IdComentario));
        }
    }
}

